import React from 'react';
import { Container } from 'react-bootstrap';
import './SigaInsta.scss';
import axios from 'axios';
import insta from './img/siga-insta.png';

export class SigaInsta extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            images: [],
            takeLast: 8,
            //escolha entre thumbnail (150), low_resolution (320), standard_resolution (640)
            quality: 'low_resolution'
        }
    }

    componentDidMount(){
        this.loadImages();
    }

   image = (url, text) => {
            return (<div class="col-md-3 col-6" >
                <div className="insta" style={{ backgroundImage: `url(${url})` }}>
                    <span class="figcaption">
                        {text.slice(0, 90)}...
                    </span>
                </div>
        </div>
    );
   }

     loadImages(){
            let REF = this;
              //procure gerador de token p/ instagram no google
              const token = "3220693671.1677ed0.bc7d3075f1034428aae32761eb61d9c9";
              //informe o insta id
              const instagram_id = "3220693671";
              const  url = "https://api.instagram.com/v1/users/"+instagram_id+"/media/recent?access_token="+token;
   
              return axios.get(url).then(function(response){
                  REF.setState({ images : response.data.data.slice(0, REF.state.takeLast) });
              });
       
    }

    render(){
        let REF = this;
        return (<section className="SigaInsta">
            <Container>
               <a href="https://instagram.com/linhaverdeeletrica">
                   <img src={insta} alt="Siga-nos no Instagram" className="d-block img-fluid follow-us" />
                   </a>
                   <div class="row">
            { 
                REF.state.images.map(function(pic){
                    let q = REF.state.quality;
                    return REF.image(pic.images[q].url, pic.caption.text);
                })
            
        }
        </div>
</Container>
</section>
)
    }
} 

export default SigaInsta;
