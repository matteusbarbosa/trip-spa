import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './About.scss';
import about from './img/about.png';
import mission from './img/mission.png';

function About() {
    return (
        <section className="About">
            <Container>
                <Row>
                    <Col md={8}>
                        <Row>
                            <Col md={6} className="my-2 my-md-none">
                                <div className="w-img">
                                    <img src={about} alt="sobre nós" className="mb-2" />
                                </div>
                                <h3 className="my-2">Sobre nós</h3>
                                <p> A Linha Verde Elétrica foi criada com intuito de suprir as necessidades da região nos segmentos de materiais elétricos prediais, comerciais e se especializou em Padrões Cemig de baixa e média tensão
                                   </p>
                            </Col>
                            <Col md={6} className="my-2 my-md-none">
                                <div className="w-img">
                                    <img src={mission} alt="missão" className="mb-2" />
                                </div>

                                <h3 className="my-2"> Nossa missão </h3>
                                <p> Fazer possível para que você seja sempre atendido com qualidade e pontualidade.</p>
                            </Col>
                        </Row>
                    </Col>

                    <Col md={4} className="sidebar">
                        <Row className="my-md-none mb-2">
                            <Col xs={4}>
                                <img src={mission} alt="missão" className="mb-2 img-fluid my-2" />
                            </Col>
                            <Col xs={8}>
                                <h5>Serviços</h5>
                                <p>Oferecemos uma ampla gama de serviços de acordo com o seu orçamento e as prioridades do seu negócio.</p>

                            </Col>
                        </Row>
                        <Row className="my-md-none my-2">
                            <Col xs={4}>
                                <img src={about} alt="sobre nós" className="mb-2 img-fluid" />
                            </Col>
                            <Col xs={8}>
                                <h5>Produtos</h5>
                                <p>Oferecemos um portfólio completo de produtos e soluções para atender todas as necessidades.</p>

                            </Col>
                        </Row>
                        <Row className="my-md-none my-2">
                            <Col xs={4}>
                                <img src={mission} alt="missão" className="mb-2 img-fluid" />
                            </Col>
                            <Col xs={8}>
                                <h5>Ofertas</h5>
                                <p>Contate-nos para obter informações sobre as nossas últimas promoções. </p>

                            </Col>
                        </Row>

                    </Col>

                </Row>
            </Container>
        </section>
    );
}

export default About;
