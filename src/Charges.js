import React from 'react';
import './Charges.scss';
import './bootstrap.min.css';
import NavbarTop from './NavbarTop.js';
import Trips from './Trips.js';
import InfoTile from './InfoTile';
import Header from './Header';
import axios from 'axios';
import {CFG, axiosHeader, qrCodeValidate} from './CFG';
import {sessionGet, setUser} from './usuario/Auth';
import {Col, Row, Button, Card, Container, Table} from 'react-bootstrap';
import {getDmyHis, objectToDate, parseUS} from './util/date';
import {mask_decimal} from './util/inputMask';
var QRCode = require('qrcode.react');

class Charges extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
          user: {
            id:0
          },
            list: [],     
            form: {}     
        };

        this.setUser = setUser.bind(this);
    
    }

    componentDidMount(){
      let email = sessionGet('email');
      this.setUser(email);
  }

  componentDidUpdate(prevProps, prevState){
    if(prevState.user.id != this.state.user.id){
      this.get();
    }
  }

    formControl = (formRef) => {
      delete formRef.form;
      delete formRef.list;
      this.setState({ form: formRef });
    }

    get = () => {
      let REF = this; 

      let url = CFG.URL_API+'/charge?user_id='+this.state.user.id;
      
    return axios.get(url, axiosHeader())
      .then(function(response){
          REF.setState({ list: response.data, loading: false });
      })
      .catch(function(error){
          console.log(error);
    //requestException(error);
});
  }
    
  render(){

    return (
            <>
                <InfoTile></InfoTile>
               <Header></Header>
        
               <Container>
               <h3 className="text-uppercase"><i className="fa fa-list"></i> Minhas reservas</h3>
               {
                        this.state.list.map((e, i) => {

                            return (
                            <Card className="my-4 my-md-2">
                            <Card.Body>

<Row>
              <Col md="3">
                <QRCode value={qrCodeValidate+'/'+this.state.id} className="d-block m-auto my-4" size={150} fgColor={"black"} />
                <span className="badge badge-info float-right">#{e.id}</span>
                </Col>
                <Col md="9">
                <Row>
                                      <Col>
                            <h5>{e.title} </h5>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col>
                                      {e.details}
                                      </Col>
                                    </Row>
                                    <Row className="mt-2">
                                      <Col md="8">
                                      PagSeguro: <span className="badge badge-success">{e.status.title}</span>
                                        <small className="text-muted ml-2">{e.status.details}</small>
                                      </Col>
                                    <Col md="4">
                                      Valor: R${mask_decimal(this.state.amount)}                                      
                                      </Col>
                                    </Row>
                                      <Row className="mt-2">
                                      <Col md="4">
                                      Compra: <u>{e.created_at_format}</u>
                                      </Col>
                                      <Col md="4">
                                      Saída: <u>{e.start_at_format}</u>
                                      </Col>
                                      <Col md="4">
                                      Volta: <u>{e.end_at_format}</u>
                                      </Col>
                                    </Row>
                </Col>
              </Row>
                              
                            </Card.Body>
                      
                            </Card>
                        )}
                        )
                    }
                    </Container>
            </>
    );
  }

}

export default Charges;
