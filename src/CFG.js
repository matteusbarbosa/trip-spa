
import React from 'react';
import {sessionGet} from './usuario/Auth';

const ENV = window.location.origin.includes("transbaixinhoviagens.com.br") ? "production" : "local";

export const CFG = (() => {

  if(ENV == 'production')
  return {
    SESSION_PREFIX: 'trip',
    IGNORE_AUTH : ['login', 'login-recuperar'],
    URL_APP : 'https://transbaixinhoviagens.com.br',
    URL_API : 'https://api.transbaixinhoviagens.com.br'
  };

  if(ENV == 'local')
  return {
    SESSION_PREFIX: 'trip',
    IGNORE_AUTH : ['login', 'login-recuperar'],
    URL_APP : 'http://192.168.100.185:3000',
    URL_API : 'http://192.168.100.185:8000'
  };

})();

export function axiosHeader(){
  return  {
    headers: {
      'Authorization': 'Bearer '+sessionGet('key') }
  }
}

export function qrCodeHash(id){

}

export function qrCodeHashDecode(hash){
    
}

export const qrCodeValidate = CFG.URL_APP+'/admin/charges/form';

export default CFG;