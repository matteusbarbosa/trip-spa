import React  from 'react';
import model from './img/model.jpeg';

function Wireframe() {
    return ( 
        <img src={model} alt="model" />
    );
}
  
export default Wireframe;
