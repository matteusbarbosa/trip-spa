import React from 'react';
import './App.css';
import './bootstrap.min.css';
import InfoTile from './InfoTile';
import Header from './Header';
import BannerSlide from './BannerSlide';
import News from './News';
import SigaInsta from './SigaInsta';
import CFG from './CFG';

class Landing extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
     
            form: {}
     
        };
    
    }
    
    formControl = (formRef) => {
      delete formRef.form;
      delete formRef.list;
      this.setState({ form: formRef });
    }
    
  render(){

    return (
            <>
               <InfoTile></InfoTile>
               <Header></Header>
               <BannerSlide></BannerSlide>
               <SigaInsta count="6"/>`
               <News count="6"/>
               
               <div className="my-4 py-4 text-center">
                 <a href={CFG.URL_APP+"/avaliacao"}><img src="/img/5stars.png" className="img-fluid m-auto d-block" /></a>
               </div>

            </>
    );
  }

}

export default Landing;
