import React from 'react';

class FooterCopyright extends React.Component  {

	render(){ 
        return (
            <div className="app-footer app-footer-default" id="footer">
            <div className="app-footer-line">
              <div className="copyright wide text-center">
                © 2019 Transbaixinho. Todos os direitos reservados. | 
                <span className="fa fa-chrome" /> <span className="fa fa-firefox" /> <span className="fa fa-safari" /> 
                | Desenvolvido por <a href="https://desenvolvedormatteus.com.br">Matteus Barbosa</a>
              </div>
            </div>
          </div>
          
        );
	}
}

export default FooterCopyright;