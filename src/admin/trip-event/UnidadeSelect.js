import React from 'react';
import axios from 'axios';
import {requestException} from '../util/exception';
import {CFG, axiosHeader} from '../CFG';
import { inputChangeHandler,inputBlurHandler } from '../CustomForm';
import {Alert} from 'react-bootstrap';

class UnidadeSelect extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            list: [['', 'Carregando opções']],
            categoria: '',
            validator: {}
        };
        this.inputChangeHandler = inputChangeHandler.bind(this);
        this.inputBlurHandler = inputBlurHandler.bind(this);
        this.get = this.get.bind(this);
        this.get();
    }

    componentDidUpdate(prevProps, prevState){
     
        if(prevState.categoria != this.props.categoria)
        this.setState({ categoria : this.props.categoria });
    
}

    render(){

        if(this.state.list.length == 1)
        return (<Alert variant="warning">
            Lista vazia
        </Alert>);

        return (
            <select className="form-control" name="unidade" value={this.props.unidade} id="f-unidade" onChange={this.inputChangeHandler} onBlur={this.inputBlurHandler}  >
             
                  {this.state.list.map((val) => (<option key={val[0]} value={val[0]}>{val[1]}</option>)) }
            </select>
        );
    }

    get(){
        let CS = this; 

        axios.get(CFG.URL_API+'/produto-unidade?selectMode=pluck&pluckKey=id&pluckValue=descricao', axiosHeader())
        .then(function(response){
            let entries = Object.entries(response.data);
            let htmlList = [];
            htmlList = entries.sort(function(a,b){
                return (a[1] > b[1]) ? 1  : -1;
            });
            entries.unshift(['', 'Selecione']);
            
            CS.setState({ list: htmlList });

          //Perform action based on response
      })
        .catch(function(error){
      requestException(error);
 });
    }

}

export default UnidadeSelect;
