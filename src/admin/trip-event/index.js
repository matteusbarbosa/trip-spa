var gridSelector = '.table-produtos';

var abrirTelaEdicao = function () {
	var id = $(this).attr('data-id');
	var urla = 'produtos/editar.php' + (id > 0 ? '?id=' + id : '');

	openViewEdicao(
		urla, 
		{ form: '#frmMeusDados', size: '800x500' }, 
		[
			{ type: 'save', label: 'Salvar', onsave: salvarTelaEdicao },
			{ type: 'cancel', label: 'Cancelar' }
		]
	);
};

var salvarTelaEdicao = function (dadosForm) {
	$.ajax({
		url: 'produtos/action.php',
		data: dadosForm,
		type: 'post',
		dataType: 'json',
		success: function (json) {
			app.alerta({
        text: json.msg,
        type: (json.status ? 'success' : 'error'),
      });
      $(gridSelector).DataTable().ajax.reload( null, false );
		},
		error: function (a,b,error) {
			app.alerta({
        text: "Erro ao processar. " + error,
        type: 'error',
      })				
		}
	});

};

$(function () {
	$(".btnCadastrar").live('click', abrirTelaEdicao);
	$(".btnEditar").live('click', abrirTelaEdicao);

	app.grid(gridSelector, 'produtos/action.php', {order: [{'column':'3','dir':'asc'}]  }); 
});