import React from 'react';
import '../Modal.scss';

import {Modal, Button} from 'react-bootstrap';
import ProdutoTab from './ProdutoTab';
import IconeThumbnail from '../icone/IconeThumbnail';
import url_btn_close from '../img/icons/close.png';

class ProdutoModal extends React.Component{

  constructor(props) {
      super(props);

    this.state = { 
        form: {},
        formFornecedores: {},
        formFormula: {},
        currentTab: "home"
      };

      this.formDelete = this.formDelete.bind(this);
  }

  updateTab = (t) => this.setState({ currentTab: t });

  modalControl = (form_comp) => {
    this.setState({ form: form_comp });
  }

  modalControlFornecedores = (form_comp) => {
    this.setState({ formFornecedores: form_comp });
  }

  modalControlFormula = (form_comp) => {
    this.setState({ formFormula: form_comp });
  }

  render () {
    return (
      <Modal
        onEscapeKeyDown={this.props.onHide}
        show={this.props.show}
        onHide={this.props.onHide, this.props.formupdate}
        size="lg"
        aria-labelledby="contained-modal-title"
      >
        <Modal.Header className="py-2">
          <Modal.Title id="contained-modal-title">
            Produto 
          </Modal.Title>
          <img className="close" src={url_btn_close} onClick={this.props.onHide} />

        </Modal.Header>
        <Modal.Body>
            <ProdutoTab
            tabupdate={this.updateTab}
            show={this.props.show}
            form={this.props.form}
            modalcontrol={this.modalControl}
            modalcontrol_fornecedores={this.modalControlFornecedores}
            modalcontrol_formula={this.modalControlFormula}
          />           
        </Modal.Body>
        <Modal.Footer>
          <Button hidden={this.state.currentTab != "home"} className="btn-danger btn-left" onClick={this.formDelete}>Excluir</Button>
          <Button onClick={this.formSubmit}>Confirmar</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  formSubmit = () =>  {

    if(
      !this.state.formFornecedores.isValid()
      || !this.state.formFormula.isValid()
      || !this.state.formFornecedores
      || !this.state.formFormula
    )
    return false;

    if(typeof this.props.tablerowupdate === "function")
    this.props.tablerowupdate(this.state.form);

    //produto
    if(this.state.form.state.id){
      this.state.form.put();
    }
    else{
      this.state.form.post();
    }
    
    this.state.formFornecedores.post();
    this.state.formFormula.post();

    this.props.onHide();  
  }

  formDelete() {

    if(typeof this.props.tablerowupdate === "function")
    this.props.tablerowupdate(this.state.form);


    let c = window.confirm("Deseja mesmo remover o produto "+this.state.form.state.produto+" ?");
    if (c == true) {
      if(this.state.form.state.id)
      this.state.form.delete();
      this.props.onHide();  
    }

  }
  
}

export default ProdutoModal;